#!/usr/bin/env bash

cpname="registry"
version=$(tools/version.sh)

cd registry
rm -f ._Version.meta
drpcli contents bundle ../${cpname}.json Version=$version
drpcli contents bundle ../${cpname}.yaml Version=$version --format=yaml
cd ..

mkdir -p rebar-catalog/${cpname}
cp ${cpname}.json rebar-catalog/${cpname}/$version.json

